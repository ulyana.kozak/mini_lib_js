const ArrayMethods = require("./mini_lib.js").ArrayMethods;
const AsChain = require("./mini_lib").AsChain;
const expect = require("chai").expect;

const arr1 = ["Banana", "Orange", "Apple", "Mango"];
const arr2 = [];
const arr3 = [1, 2, 3, 4];
const arr4 = [true, false, true];
const arr5 = [9, 8, 7, 6, 5, 4, 3, 2, 1];
const bool1 = true;
const bool2 = false;
const fun1 = function () {
  return "hello";
};
const date1 = new Date(2014, 11, 24);
const obg1 = { name: "Una", "b-day": "18.03.2003" };
const und1 = undefined;
const null1 = null;
const numb1 = 15226;
const str = "string";

describe("mini_lib", () => {
  describe("isArray", () => {
    it("should return true if obg is array", (done) => {
      let isArray = new ArrayMethods();
      expect(isArray.isArray(arr1)).to.deep.equal(true);
      expect(isArray.isArray(arr2)).to.deep.equal(true);
      expect(isArray.isArray(arr3)).to.deep.equal(true);
      done();
    });

    it("should return false if obj is not array", () => {
      let isArray = new ArrayMethods();
      expect(isArray.isArray(bool1)).to.deep.equal(false);
      expect(isArray.isArray(bool2)).to.deep.equal(false);
      expect(isArray.isArray(fun1)).to.deep.equal(false);
      expect(isArray.isArray(date1)).to.deep.equal(false);
      expect(isArray.isArray(obg1)).to.deep.equal(false);
      expect(isArray.isArray(und1)).to.deep.equal(false);
      expect(isArray.isArray(null1)).to.deep.equal(false);
      expect(isArray.isArray(numb1)).to.deep.equal(false);
      expect(isArray.isArray(str)).to.deep.equal(false);
    });
  });

  describe("isBoolean", () => {
    it("should return true if obg is boolean", () => {
      let isBoolean = new ArrayMethods();
      expect(isBoolean.isBoolean(bool1)).to.deep.equal(true);
      expect(isBoolean.isBoolean(bool2)).to.deep.equal(true);
    });

    it("should return false if obj is not boolean", () => {
      let isBoolean = new ArrayMethods();
      expect(isBoolean.isBoolean(arr1)).to.deep.equal(false);
      expect(isBoolean.isBoolean(arr2)).to.deep.equal(false);
      expect(isBoolean.isBoolean(fun1)).to.deep.equal(false);
      expect(isBoolean.isBoolean(date1)).to.deep.equal(false);
      expect(isBoolean.isBoolean(obg1)).to.deep.equal(false);
      expect(isBoolean.isBoolean(und1)).to.deep.equal(false);
      expect(isBoolean.isBoolean(null1)).to.deep.equal(false);
      expect(isBoolean.isBoolean(numb1)).to.deep.equal(false);
      expect(isBoolean.isBoolean(str)).to.deep.equal(false);
    });
  });

  describe("isDate", () => {
    it("should return true if obg is date", () => {
      let isDate = new ArrayMethods();
      expect(isDate.isDate(date1)).to.deep.equal(true);
    });

    it("should return false if obj is not date", () => {
      let isDate = new ArrayMethods();
      expect(isDate.isDate(arr1)).to.deep.equal(false);
      expect(isDate.isDate(arr2)).to.deep.equal(false);
      expect(isDate.isDate(fun1)).to.deep.equal(false);
      expect(isDate.isDate(bool1)).to.deep.equal(false);
      expect(isDate.isDate(bool2)).to.deep.equal(false);
      expect(isDate.isDate(obg1)).to.deep.equal(false);
      expect(isDate.isDate(und1)).to.deep.equal(false);
      expect(isDate.isDate(null1)).to.deep.equal(false);
      expect(isDate.isDate(numb1)).to.deep.equal(false);
      expect(isDate.isDate(str)).to.deep.equal(false);
    });
  });

  describe("isFunction", () => {
    it("should return true if obg is Function", () => {
      let isFunction = new ArrayMethods();
      expect(isFunction.isFunction(fun1)).to.deep.equal(true);
    });

    it("should return false if obj is not Function", () => {
      let isFunction = new ArrayMethods();
      expect(isFunction.isFunction(arr1)).to.deep.equal(false);
      expect(isFunction.isFunction(arr2)).to.deep.equal(false);
      expect(isFunction.isFunction(date1)).to.deep.equal(false);
      expect(isFunction.isFunction(bool1)).to.deep.equal(false);
      expect(isFunction.isFunction(bool2)).to.deep.equal(false);
      expect(isFunction.isFunction(obg1)).to.deep.equal(false);
      expect(isFunction.isFunction(und1)).to.deep.equal(false);
      expect(isFunction.isFunction(null1)).to.deep.equal(false);
      expect(isFunction.isFunction(numb1)).to.deep.equal(false);
      expect(isFunction.isFunction(str)).to.deep.equal(false);
    });
  });

  describe("isNull", () => {
    it("should return true if obg is Null", () => {
      let isNull = new ArrayMethods();
      expect(isNull.isNull(null1)).to.deep.equal(true);
    });

    it("should return false if obj is not Null", () => {
      let isNull = new ArrayMethods();
      expect(isNull.isNull(arr1)).to.deep.equal(false);
      expect(isNull.isNull(arr2)).to.deep.equal(false);
      expect(isNull.isNull(fun1)).to.deep.equal(false);
      expect(isNull.isNull(bool1)).to.deep.equal(false);
      expect(isNull.isNull(bool2)).to.deep.equal(false);
      expect(isNull.isNull(obg1)).to.deep.equal(false);
      expect(isNull.isNull(und1)).to.deep.equal(false);
      expect(isNull.isNull(date1)).to.deep.equal(false);
      expect(isNull.isNull(numb1)).to.deep.equal(false);
      expect(isNull.isNull(str)).to.deep.equal(false);
    });
  });

  describe("isNumber", () => {
    it("should return true if obg is Number", () => {
      let isNumber = new ArrayMethods();
      expect(isNumber.isNumber(numb1)).to.deep.equal(true);
    });

    it("should return false if obj is not Number", () => {
      let isNumber = new ArrayMethods();
      expect(isNumber.isNumber(arr1)).to.deep.equal(false);
      expect(isNumber.isNumber(arr2)).to.deep.equal(false);
      expect(isNumber.isNumber(fun1)).to.deep.equal(false);
      expect(isNumber.isNumber(bool1)).to.deep.equal(false);
      expect(isNumber.isNumber(bool2)).to.deep.equal(false);
      expect(isNumber.isNumber(obg1)).to.deep.equal(false);
      expect(isNumber.isNumber(und1)).to.deep.equal(false);
      expect(isNumber.isNumber(date1)).to.deep.equal(false);
      expect(isNumber.isNumber(null1)).to.deep.equal(false);
      expect(isNumber.isNumber(str)).to.deep.equal(false);
    });
  });

  describe("isString", () => {
    it("should return true if obg is String", () => {
      let isString = new ArrayMethods();
      expect(isString.isString(str)).to.deep.equal(true);
    });

    it("should return false if obj is not String", () => {
      let isString = new ArrayMethods();
      expect(isString.isString(arr1)).to.deep.equal(false);
      expect(isString.isString(arr2)).to.deep.equal(false);
      expect(isString.isString(fun1)).to.deep.equal(false);
      expect(isString.isString(bool1)).to.deep.equal(false);
      expect(isString.isString(bool2)).to.deep.equal(false);
      expect(isString.isString(obg1)).to.deep.equal(false);
      expect(isString.isString(und1)).to.deep.equal(false);
      expect(isString.isString(date1)).to.deep.equal(false);
      expect(isString.isString(null1)).to.deep.equal(false);
      expect(isString.isString(numb1)).to.deep.equal(false);
    });
  });

  describe("isUndefined", () => {
    it("should return true if obg is Underfined", () => {
      let isUndefined = new ArrayMethods();
      expect(isUndefined.isUndefined(und1)).to.deep.equal(true);
    });

    it("should return false if obj is not Undefined", () => {
      let isUndefined = new ArrayMethods();
      expect(isUndefined.isUndefined(arr1)).to.deep.equal(false);
      expect(isUndefined.isUndefined(arr2)).to.deep.equal(false);
      expect(isUndefined.isUndefined(fun1)).to.deep.equal(false);
      expect(isUndefined.isUndefined(bool1)).to.deep.equal(false);
      expect(isUndefined.isUndefined(bool2)).to.deep.equal(false);
      expect(isUndefined.isUndefined(obg1)).to.deep.equal(false);
      expect(isUndefined.isUndefined(str)).to.deep.equal(false);
      expect(isUndefined.isUndefined(date1)).to.deep.equal(false);
      expect(isUndefined.isUndefined(null1)).to.deep.equal(false);
      expect(isUndefined.isUndefined(numb1)).to.deep.equal(false);
    });
  });

  describe("FirstElem", () => {
    it("should return first element of array", () => {
      let first = new ArrayMethods();
      expect(first.first(arr1)).to.deep.equal("Banana");
      expect(first.first(arr2)).to.deep.equal(undefined);
      expect(first.first(arr3)).to.deep.equal(1);
      expect(first.first(arr4)).to.deep.equal(true);
    });
  });

  describe("LastElem", () => {
    it("should return last element of array", () => {
      let last = new ArrayMethods();
      expect(last.last(arr1)).to.deep.equal("Mango");
      expect(last.last(arr2)).to.deep.equal(undefined);
      expect(last.last(arr3)).to.deep.equal(4);
      expect(last.last(arr4)).to.deep.equal(true);
    });
  });

  describe("SkipElem", () => {
    it("should return array without element which has index = number", () => {
      let skip = new ArrayMethods();
      expect(skip.skip(arr2, 1)).to.deep.equal([]);
      expect(skip.skip(arr3, 3)).to.deep.equal([1, 2, 3]);
      expect(skip.skip(arr5, 2)).to.deep.equal([9, 8, 6, 5, 4, 3, 2, 1]);
    });
  });

  describe("TakeElem", () => {
    it("should return element of index = number", () => {
      let take = new ArrayMethods();
      expect(take.take(arr1, 1)).to.deep.equal("Orange");
      expect(take.take(arr3, 2)).to.deep.equal(3);
      expect(take.take(arr4, 1)).to.deep.equal(false);
      expect(take.take(arr5, 4)).to.deep.equal(5);
    });
  });
});

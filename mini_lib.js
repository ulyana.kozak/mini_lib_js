class ArrayMethods {
  isArray(obj) {
    if (obj == null || obj == undefined) {
      return false;
    } else if (obj.constructor === Array) {
      return true;
    } else {
      return false;
    }
  }

  isBoolean(obj) {
    if (obj == null || obj == undefined) {
      return false;
    } else if (typeof obj == "boolean") {
      return true;
    } else {
      return false;
    }
  }

  isDate(obj) {
    if (obj == null || obj == undefined) {
      return false;
    } else if (obj.constructor === Date) {
      return true;
    } else {
      return false;
    }
  }

  isFunction(obj) {
    if (obj == null || obj == undefined) {
      return false;
    } else if (obj.constructor === Function) {
      return true;
    } else {
      return false;
    }
  }

  isNull(obj) {
    if (obj === null) {
      return true;
    } else {
      return false;
    }
  }

  isNumber(obj) {
    if (obj == null || obj == undefined) {
      return false;
    } else if (typeof obj == "number") {
      return true;
    } else {
      return false;
    }
  }

  isString(obj) {
    if (obj == null || obj == undefined) {
      return false;
    } else if (obj.constructor === String) {
      return true;
    } else {
      return false;
    }
  }

  isUndefined(obj) {
    if (typeof obj == "undefined") {
      return true;
    } else {
      return false;
    }
  }

  first(arr) {
    return arr.at(0);
  }

  last(arr) {
    return arr.at(-1);
  }

  skip(arr, number) {
    let newArr = arr.filter((el) => el != arr[number]);
    return newArr;
  }

  take(arr, number) {
    let newArr = arr.find((el) => el === arr[number]);
    return newArr;
  }
}

let AsChain = {
  result: [],
  number: 0,
  asChain: function (arr) {
    this.result = arr;
    this.number = this.result.length - 1;
    return this;
  },
  skip: function (func) {
    this.result = func(this.result, this.number);
    this.number = this.result.length - 1;
    return this;
  },
  take: function (func) {
    this.result = func(this.result, this.number);
    this.number = this.result.length - 1;
    return this;
  },
  print: function () {
    console.log(this.result);
    return this;
  },
};

arrMethods = new ArrayMethods();

AsChain.asChain([1, 2, 3, 5])
  .print()
  .skip(arrMethods.skip)
  .print()
  .take(arrMethods.take)
  .print();


module.exports = { ArrayMethods };

# mini_lib_js

## Info

## The library provides the following functionality:

<hr>bool isArray(obj) should return true if obg is array
<hr>bool isBoolean(obj) should return true if obg is boolean
<hr>bool isDate(obj) should return true if obg is date
<hr>bool isNumber(obj) should return true if obg is Number
<hr>bool isString(obj) should return true if obg is String
<hr>bool isFunction(obj) should return true if obg is Function
<hr>bool isUndefined(obj) should return true if obg is Underfined
<hr>bool isNull(obj) should return true if obg is Null


## Working with arrays:

<hr>obj first(arr) should return first element of array
<hr>obj last(arr) should return last element of array
<hr>newArr skip(arr, number) should return array without element which has index = number
<hr>newArr take(arr, number) should return element of index = number

## Chaining:
Implement the ability to create chains:
<hr>asChain(arr).skip(func).take(func)

## Getting started

To make it easy for you to get started with  mini library, here's a list of recommended next steps.

## Install Git on your machine

Use commands provided by gitHub 

## CLone this repository 

You can use either SSH or HTML key 

## Make folder, which will contain mini_library
   $ mkdir
## Use special command in your terminal
   $ git clone
## That's all! I hope you'll enjoy it :) 